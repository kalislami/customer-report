// ready function
$(document).ready(function() {
  document.getElementById('title').innerHTML = 'Home';
  $('#content-table').hide();
  $('#content-home').show();

  $('#otcost-tbl').dataTable({
    "ajax": {
        'type': 'POST',
        'url': 'getCostumer.php',
        'data': {
           id: $('#session_id_global').val(),
           field: 'id_sales',
           param: 'otherCostumer'
        }
    },
    "columns": [
            { "data": "numb" },
            { "data": "sales_name" },
            { "data": "cost_name" },
            { "data": "job_title" },
            { "data": "company" },
            { "data": null,
              "render":function(data,type,row) {
                  return data.address.substr( 0, 15 )+'. . .'
                }
            },
            { "data": "city" },
            { "data": "postal_code" },
            { "data": "phone" },
            { "data": "fax" },
            { "data": "email" },
            {
              sortable: false,
              "render": function ( data, type, full, meta ) {
                return "<a class='btn btn-secondary tbl-act' href='"+"javascript:view("+full.id_costumer+")"+"'><i class='fa fa-eye'></i></a>";
              }
            }
        ]
    
  });

  $('#mycost-tbl').dataTable({
    "ajax": {
        'type': 'POST',
        'url': 'getCostumer.php',
        'data': {
           id: $('#session_id_global').val(),
           field: 'id_sales',
           param: 'MyCustomer'
        }
    },
    "columns": [
            { "data": "numb" },
            { "data": "name" },
            { "data": "job_title" },
            { "data": "company" },
            { "data": null,
              "render":function(data,type,row) {
                  return data.address.substr( 0, 15 )+'. . .'
                }
            },
            { "data": "city" },
            { "data": "postal_code" },
            { "data": "phone" },
            { "data": "fax" },
            { "data": "email" },
            {
              sortable: false,
              "render": function ( data, type, full, meta ) {
                return "<a class='btn btn-secondary tbl-act' href='"+"javascript:view("+full.id_costumer+")"+"'><i class='fa fa-eye'></i></a>"
                    +" <a class='btn btn-info tbl-act' href='javascript:edit("+full.id_costumer+")'><i class='fa fa-pencil'></i></a>"
                    +" <a class='btn btn-danger tbl-act' href='javascript:del("+full.id_costumer+")'><i class='fa fa-trash'></i></a>";
              }
            }
        ]
  });

  $("#btn-changePwd").click(function(){
    $('#changPwdModal').modal('show');
  });
   $("#editProfile").click(function(){
    $('#myProfileModal').modal('hide');
    $('#myProfileEditModal').modal('show');
  });
  $("#btn-MyProf").click(function(){
    $('#myProfileModal').modal('show');
  });
  $("#btn-newcostInput").click(function(){
    $('#addModal').modal('show');
  });
  $("#btn-newcostExcel").click(function(){
    $('#addModalExcel').modal('show');
  });
  $("#btn-otcostBySales").click(function(){
    $('#listSalesModal').modal('show');
  });
  $("#btn-home").click(function(){
    document.getElementById('title').innerHTML = 'Home';
    $('#content-table').hide();
    $('#content-home').show();
  });
  $("#btn-mycost").click(function(){
    document.getElementById('title').innerHTML = 'My Customer';
    $('#content-table').show();
    $('#content-home').hide();
    $('#tbl-mycost').show();
    $('#tbl-otcost').hide();
    $('#tbl-costBySales').hide();
  });
  $("#btn-otcost").click(function(){
    document.getElementById('title').innerHTML = 'All Others Customer';
    $('#content-table').show();
    $('#content-home').hide();
    $('#tbl-mycost').hide();
    $('#tbl-otcost').show();
    $('#tbl-costBySales').hide();
  });

  // button update costumer
  $("#updateCost").click(function(){

    var id = $('#id_cost').val();
    var nama = $('#cost_name_edit').val();
    var job_title = $('#job_title_edit').val();
    var company = $('#company_edit').val();
    var address = $('#address_edit').val();
    var city = $('#city_edit').val();
    var postal_code = $('#post_code_edit').val();
    var phone = $('#phone_edit').val();
    var fax = $('#fax_edit').val();
    var email = $('#email_edit').val();

    $.ajax({
       url: 'insertCostumer.php',
       type: "POST",
       dataType:'json',
       data: ({ id: id,
                nama: nama,
                job_title: job_title,
                company: company,
                address: address,
                city: city,
                postal_code: postal_code,
                phone: phone,
                fax: fax,
                email: email,
              }),
       success: function(){
       }
    });
    swal('Updated!', 'Your data successfully updated.', 'success');
    $('#editModal').modal('hide');
    // $("#mycost-tbl").load(location.href + " #mycost-tbl");
    window.location.reload();
  }); // button update costumer

  // button add costumer
  $("#addCostInput").click(function(){
    var nama = $('#cost_name').val();
    var job_title = $('#job_title').val();
    var company = $('#company').val();
    var address = $('#address').val();
    var city = $('#city').val();
    var postal_code = $('#post_code').val();
    var phone = $('#phone').val();
    var fax = $('#fax').val();
    var email = $('#email').val();
    var id_sales = $('#id_sales_add').val();
   
    $.ajax({
       url: 'insertCostumer.php',
       type: "POST",
       dataType:'json',
       data: ({ nama: nama,
                job_title: job_title,
                company: company,
                address: address,
                city: city,
                postal_code: postal_code,
                phone: phone,
                fax: fax,
                email: email,
                id_sales: id_sales,
              }),
       success: function(){
       }
    });
    swal("Successfully Add Data!", "your data has succesfully inserted", "success");

    $('#addModal').modal('hide');
    window.location.reload();
  }); // button add costumer

  // ganti password user  
  $("#updatePwd").click(function(){
    var id = $('#id_sales_pwd').val();
    var oldpwd = $('#pwd').val();
    var opRT = $('#old_pass').val();
    var newpwd = $('#new_pass').val();
    var newpwdRT = $('#new_pass_rt').val();
    var oldpwdRT = CryptoJS.MD5(opRT);

    if (oldpwd != oldpwdRT) {
      swal("Old password is wrong !", "make sure your old password is correct", "warning");
      return false;
    }else if (newpwd != newpwdRT) {
      swal("New password and Retype password is not same!", "make sure your new password and retype password is same", "warning");
      return false;
    }else{
      $.ajax({
         url: 'changePassword.php',
         type: "POST",
         dataType:'json',
         data: ({ id: id,
                  pwd: newpwd,
                }),
         success: function(){
         }
      });
      swal({title: 'Updated!', text: 'Your password has been changed.', type: 'success'},
         function(){ 
          // window.location.reload();
        }); 
      window.location.reload();
      $('#changPwdModal').modal('hide');
    }
  });// ganti password user

   // update profile user  
  $("#updateProfile").click(function(){
    // var id = $('#id_myprof').val();
    // var nama = $('#myname_edit').val();
    // var otemail = $('#otherEmail_edit').val();
    // var divisi = $('#mydiv_edit').val();
    // var jabatan = $('#myjob_edit').val();
    // var phone = $('#myphone_edit').val();

    var id = document.getElementById("id_myprof").value;
    var nama = document.getElementById("myname_edit").value;
    var otemail = document.getElementById("otherEmail_edit").value;
    var divisi = document.getElementById("mydiv_edit").value;
    var jabatan = document.getElementById("myjob_edit").value;
    var phone = document.getElementById("myphone_edit").value;

    $.ajax({
       url: 'changeProfile.php',
       type: "POST",
       dataType:'json',
       data: ({ id: id,
                nama: nama,
                divisi: divisi,
                jabatan: jabatan,
                phone: phone,
                otemail: otemail
              }),
       success: function(){
       }
    });
    swal({title: 'Updated!', text: 'Your profile has been changed.', type: 'success'},
       function(){ 
        // window.location.reload();
      }); 
    window.location.reload();
    $('#myProfileEditModal').modal('hide');
    
  });// update profile user

  // export excel by sales name
  $("#btn-costumerBySales").click(function(){
    var id          = $('#sess_id').val();
    var param       = $('#param').val();
    var sales_name  = $('#sales_name').val();

    $.ajax({
       url: 'exportExcel.php',
       type: "POST",
       dataType:'json',
       data: ({ sess_id: id,
                param: param,
                sales_name: sales_name,
              }),
       success: function(){
       }
    });
    swal({title: 'Succesfully Export data !', text: "open C:/Users/Public/Downloads/_"+sales_name+"'s-Costumer-Report.csv", type: 'success'},
         function(){ 
          window.location.reload();
        }); 
  });  // export excel by sales name
}); //end ready function

//tampilan edit modal
function edit(id){
   $.ajax({
     url: 'getCostumer.php',
     type: "POST",
     dataType:'json',
     data: ({id: id, field: 'id_costumer', param: 'data_edit'}),
     success: function(data){
        console.log(data);
        $.each(data, function() {
          $('#id_cost').val(this['id_costumer']);
          $('#cost_name_edit').val(this['name']);
          $('#job_title_edit').val(this['job_title']);
          $('#company_edit').val(this['company']);
          $('#address_edit').val(this['address']);
          $('#city_edit').val(this['city']);
          $('#post_code_edit').val(this['postal_code']);
          $('#phone_edit').val(this['phone']);
          $('#fax_edit').val(this['fax']);
          $('#email_edit').val(this['email']);
        });
     }
  }); 
  $('#editModal').modal('show');
};//end tampilan edit modal

// action hapus data costumer
function del(id){
  $.ajax({
     url: 'delCostumer.php',
     type: "POST",
     dataType:'json',
     data: ({id: id}),
     success: function(){
     }
  });

  swal("Deleted!", "Your data has been deleted.", "success");

  // $("#mycost-tbl").load(location.href + " #mycost-tbl");
  window.location.reload();
} //end action hapus data costumer

// tampil data costumer berdasarkan sales
function viewCostBySales(id, nama){

  $('#costBySales-tbl').dataTable( {
    "ajax": {
        'type': 'POST',
        'url': 'getCostumer.php',
        'data': {
           id: id,
           field: 'id_sales',
           param: 'costumerBySales'
        },
    },
    "columns": [
            { "data": "numb" },
            { "data": "name" },
            { "data": "job_title" },
            { "data": "company" },
            { "data": null,
              "render":function(data,type,row) {
                  return data.address.substr( 0, 15 )+'. . .'
                }
            },
            { "data": "city" },
            { "data": "postal_code" },
            { "data": "phone" },
            { "data": "fax" },
            { "data": "email" },
            {
              sortable: false,
              "render": function ( data, type, full, meta ) {
                return "<a class='btn btn-secondary tbl-act' href='"+"javascript:view("+full.id_costumer+")"+"'><i class='fa fa-eye'></i></a>";
              }
            }
        ]
  });
   
  $('#sess_id').val(id);
  $('#sales_name').val(nama);
  $('#listSalesModal').modal('hide');
  $('#content-table').show();
  $('#tbl-mycost').hide();
  $('#tbl-otcost').hide();
  $('#tbl-costBySales').show();
  $('#content-home').hide();
  document.getElementById('title').innerHTML = nama+"'s Customer";
}; //end tampil data costumer berdasarkan sales

// tampil detail customer modal
function view(id){
  // alert(id);
  $.ajax({
     url: 'getCostumer.php',
     type: "POST",
     dataType:'json',
     data: ({id: id, field: 'id_costumer', param: 'data_edit'}),
     success: function(data){
        console.log(data);
        $.each(data, function() {
          $('#cost_name_view').html(this['name']);
          $('#job_title_view').html(this['job_title']);
          $('#company_view').html(this['company']);
          $('#address_view').html(this['address']);
          $('#city_view').html(this['city']);
          $('#post_code_view').html(this['postal_code']);
          $('#phone_view').html(this['phone']);
          $('#fax_view').html(this['fax']);
          $('#email_view').html(this['email']);
        });
     }
  }); 
  $('#viewModal').modal('show');
}; // tampil detail customer modal