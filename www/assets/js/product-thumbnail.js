 // product thumbnail images ============================================
//product image set first
$(document).ready(function(){
    var currentImg = $('.thumbnail-indicator').first().attr('href');
    $('.img-product').attr('src',currentImg);

    $('#leftScroll').hide();    
    //check scroll potition and show hide button
    $('.scroll').scroll(function(){
        console.log($(this).innerWidth());
        console.log($(this).scrollLeft());

        if($(this).scrollLeft() == 0){
            $('#leftScroll').hide();
        }else{
            $('#leftScroll').show();        
        }
        
        if($(this).scrollLeft() >= $(this).width()){
            $('#rightScroll').hide();        
        }else{
            $('#rightScroll').show();        
        }
    });
});

//if thumbnail on click
$('a.thumbnail-indicator').click(function(e){
    e.preventDefault();
    $('.img-product').attr('src',$(this).attr('href'));                
})

//if left button on click
$('#leftScroll').click(function(e){
    e.preventDefault();
    $('.scroll').animate({
        scrollLeft: "-=225px"
    },'slow');
});

//if right button on click
$('#rightScroll').click(function(e){
    e.preventDefault();
    $('.scroll').animate({
        scrollLeft: "+=225px"
    },'slow');
})

