<body>
<script src="assets/js/sweetalert2.all.min.js"></script>
<?php
	date_default_timezone_set("Asia/Jakarta");
	require 'config.php';
	$sess_id 	= $_POST['sess_id'];
 	$param 		= $_POST['param'];

 	if ($param == 'myCostumer') {
 		
 		$date = date("Y-m-d");
	  	$header = "CUSTOMER NAME,JOB TITLE,COMPANY,ADDRESS,CITY,POSTAL CODE,PHONE,FAX,EMAIL";
	  	$sql = "SELECT name, job_title, company, address, city, postal_code, phone, fax, email
	          	FROM customer WHERE id_sales = ".$sess_id." Order By id_costumer DESC";
	     
	 	$result = mysqli_query($koneksi,$sql);

	  	$file = fopen("C:\Users\Public\Downloads\My-Customer-Report-".$date.".csv","w");

	  	fputcsv($file,explode(',',$header));

	  	foreach ($result as $line){ 
	      fputcsv($file,$line);
	    }
	    
	  	// echo "<script>
	   //    	alert('Succesfully Export to C:/Users/Public/Downloads/My-Costumer-Report-".$date.".csv')
	   //    	window.location = \"index.php\"
	   //  	</script>";

	    echo "<script>
	  			swal({title: 'Succesfully Export data !', text: 'open C:/Users/Public/Downloads/My-Customer-Report-".$date.".csv', type: 'success'},
				   function(){ 
					});	
	    	</script>";

	  	fclose($file);

	  	echo "<a style='text-decoration: none;' href='index.php'><h2 align='center'><< Back</h2></a>";

 	}elseif ($param == 'otherCostumer') {

 		$date = date("Y-m-d");
	  	$header = "CUSTOMER NAME,SALES NAME,JOB TITLE,COMPANY,ADDRESS,CITY,POSTAL CODE,PHONE,FAX,EMAIL";
	  	$sql = "SELECT c.name as cost_name, s.name, c.job_title, c.company, c.address, c.city, c.postal_code, c.phone, c.fax, c.email
	          	FROM customer c LEFT JOIN sales s ON c.id_sales=s.id_sales 
	          	WHERE c.id_sales != ".$sess_id." Order By c.id_costumer DESC";
	     
	 	$result =  mysqli_query($koneksi,$sql);

	  	$file = fopen("C:\Users\Public\Downloads\Other-Customer-Report-".$date.".csv","w");

	  	fputcsv($file,explode(',',$header));

	  	foreach ($result as $line){ 
	      fputcsv($file,$line);
	    }
	    
	  	echo "<script>
	  			swal({title: 'Succesfully Export data !', text: 'open C:/Users/Public/Downloads/Other-Customer-Report-".$date.".csv', type: 'success'},
				   function(){ 
					});	
	    	</script>";

	  	fclose($file);

	  	echo "<a style='text-decoration: none;' href='index.php'><h2 align='center'><< Back</h2></a>";
 		
 	}elseif ($param == 'costumerBySales') {
 		$sales_name = $_POST['sales_name'];
 		$date = date("Y-m-d");
	  	$header = "CUSTOMER NAME,JOB TITLE,COMPANY,ADDRESS,CITY,POSTAL CODE,PHONE,FAX,EMAIL";
	  	$sql = "SELECT name, job_title, company, address, city, postal_code, phone, fax, email
	          	FROM customer WHERE id_sales = ".$sess_id." Order By id_costumer DESC";
	     
	 	$result =  mysqli_query($koneksi,$sql);

	  	$file = fopen("C:\Users\Public\Downloads\_".$sales_name."'s-Customer-Report-".$date.".csv","w");

	  	fputcsv($file,explode(',',$header));

	  	foreach ($result as $line){ 
	      fputcsv($file,$line);
	    }
	    
	  	fclose($file);	

 	}else{

	    echo "<script>
	  			swal({title: 'Connection Failed !', type: 'warning'},
				   function(){ 
					});	
	    	</script>";

	    echo "<a style='text-decoration: none;' href='index.php'><h2 align='center'><< Back</h2></a>";
 	}
?>
</body>