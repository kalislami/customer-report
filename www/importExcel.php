<body>
<script src="assets/js/sweetalert2.all.min.js"></script>
<?php
require 'config.php';
date_default_timezone_set("Asia/Jakarta");

$id_sales = $_POST['id_sales_excel'];
$created_at = date("Y-m-d H:i:s");

if(isset($_POST["addCostExcel"])){

	echo $filename = $_FILES["file"]["tmp_name"];

	if($_FILES["file"]["size"] > 0)
	{

		$file = fopen($filename, "r");
		fgetcsv($file); //to skip insert header
		while (($emapData = fgetcsv($file, 10000, ",")) !== FALSE)
		{

			//It wiil insert a row to our subject table from our csv file`
			$sql = "INSERT into customer (`name`, `job_title`, `company`, `address`, city, `postal_code`, `phone`, `fax`, `email`, `id_sales`, `created_at`) 
			values('$emapData[0]','$emapData[1]','$emapData[2]','$emapData[3]','$emapData[4]','$emapData[5]','$emapData[6]','$emapData[7]','$emapData[8]','".$id_sales."','".$created_at."')";
			//we are using mysql_query function. it returns a resource on true else False on error

			$result = mysqli_query($koneksi,$sql);
			if(! $result )
			{

				echo "<script>
			  			swal({title: 'Invalid File !', text: 'Please Upload CSV File.', type: 'warning'},
						   function(){ 
							});	
			    	</script>";

				echo "<a style='text-decoration: none;' href='index.php'><h2 align='center'><< Back</h2></a>";
			}
		}
		fclose($file);
		//throws a message if data successfully imported to mysql database from excel file
		echo "<script>
	  			swal({title: 'CSV File has been successfully Imported !', text: '', type: 'success'},
				   function(){ 
					});	
	    	</script>";

		echo "<a style='text-decoration: none;' href='index.php'><h2 align='center'><< Back</h2></a>";
		//close of connection
		mysqli_close($koneksi); 
	}
} 
?>
</body>