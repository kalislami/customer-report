<?php require'session.php'; ?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>Customer Report</title>

    <link href="assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/css/styles.css" rel="stylesheet">

    <link href="assets/lib/font-awesome/css/font-awesome.min.css" rel="stylesheet" >
  </head>

  <body>
    <script src="assets/js/jquery-3.2.1.slim.min.js"></script>
    <script src="assets/js/jquery-1.12.4.js"></script>
    <script src="assets/js/jquery.dataTables.min.js"></script>
    <script src="assets/js/popper.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/dataTables.bootstrap4.min.js"></script>
    <script src="assets/js/sweetalert2.all.min.js"></script>
    <script src="md5.js"></script>
    <script src="act.js"></script>
    <nav class="navbar navbar-expand-lg navbar-light sticky-top bg-costum">
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#nav-content" aria-controls="nav-content" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <!-- Brand -->
      <a class="navbar-brand" href="<?php echo $_SERVER["REQUEST_URI"];?>"><img class="logo" src="assets/img/JMT-logo.png"></a>

      <!-- Links -->
      <div class="collapse navbar-collapse justify-content-end" id="nav-content">
        <a class="navbar-brand text-left" href="<?php echo $_SERVER["REQUEST_URI"];?>"><i class="fa fa-refresh" aria-hidden="true"></i></a>
        <ul class="navbar-nav">
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" data-toggle="dropdown" id="user_name" href="#" role="button" aria-haspopup="true" aria-expanded="false">Hi, <?php echo $sess_name; ?></a>
            <div class="dropdown-menu" aria-labelledby="Preview">
              <button class="dropdown-item" id="btn-MyProf"><i class="fa fa-user-plus" aria-hidden="true"></i> My Profile</button>
              <button class="dropdown-item" id="btn-changePwd"><i class="fa fa-key" aria-hidden="true"></i> Change Password</button>
              <a href="logout.php" class="dropdown-item" id="btn-signout"><i class="fa fa-sign-out" aria-hidden="true"></i> Sign Out</a>
            </div>
          </li>
        </ul>
      </div>
    </nav>
    <div class="container-fluid">
      <div class="row" style="min-height: 600px;">
        <div class="col-md-2 col-xs-1 p-l-0 p-r-0" id="sidebar">
          <div class="list-group panel">
            <button class="list-group-item collapsed btn btn-secondary" id="btn-home">
              <i class="fa fa-home" aria-hidden="true"></i> Home
            </button>
            <button class="list-group-item collapsed btn btn-secondary" id="btn-mycost">
              <i class="fa fa-user" aria-hidden="true"></i> My Customer
            </button>
            <a href="#otcost-menu" class="list-group-item collapsed btn btn-secondary dropdown-toggle" data-toggle="collapse" data-parent="#sidebar" aria-expanded="false">
                <i class="fa fa-users" aria-hidden="true"></i> <span class="hidden-sm-down">Other Customer</span>
            </a>
            <div class="collapse" id="otcost-menu">
              <button class="list-group-item collapsed btn btn-secondary" id="btn-otcost">
                <i class="fa fa-eye" aria-hidden="true"></i> All Customer
              </button>
              <button class="list-group-item collapsed btn btn-secondary" id="btn-otcostBySales">
                <i class="fa fa-eye" aria-hidden="true"></i> View By Sales
              </button>
            </div>
            <a href="#menu1" class="list-group-item collapsed btn btn-secondary dropdown-toggle" data-toggle="collapse" data-parent="#sidebar" aria-expanded="false">
                <i class="fa fa-plus"></i> <span class="hidden-sm-down">Add New Customer</span>
            </a>
            <div class="collapse" id="menu1">
              <button class="list-group-item btn btn-info" data-parent="#menu1" id="btn-newcostInput">
                <i class="fa fa-pencil-square-o" aria-hidden="true"></i> Input
              </button>
              <button class="list-group-item btn btn-info" data-parent="#menu1" id="btn-newcostExcel">
                <i class="fa fa-file-excel-o" aria-hidden="true"></i> Upload Excel
              </button>
            </div>
          </div>
        </div>
        <main class="col-md-10 col-xs-11 p-l-2 p-t-2 no-padding">
          <div class="title">
            <h2 id="title" class="text-center"></h2>
          </div>
          <div id="content-table">
            <div id="tbl-mycost" class="table-responsive">
              <div class="head-table">
                <form action="exportExcel.php" method="POST">
                  <input type="hidden" name="sess_id" value="<?php echo $sess_id; ?>">
                  <input type="hidden" name="param" value="myCostumer">
                  <button class="btn btn-excel"><i class="fa fa-file-excel-o" aria-hidden="true"></i> Export Excel</button>  
                </form>
              </div>
              <table id="mycost-tbl" class="table table-striped table-bordered" cellspacing="0" width="1800px">
                <thead>
                    <tr>
                      <th>No</th>
                      <th>Name</th>
                      <th>Job Title</th>
                      <th>Company</th>
                      <th>Address</th>
                      <th>City</th>
                      <th>Postal Code</th>
                      <th>Phone</th>
                      <th>Fax</th>
                      <th>Email</th>
                      <th width="100px">Action</th>
                    </tr>
                  </thead>
              </table>
            </div>    
            <div id="tbl-otcost" class="table-responsive">
              <div class="head-table">
                <form action="exportExcel.php" method="POST">
                  <input type="hidden" name="sess_id" value="<?php echo $sess_id; ?>">
                  <input type="hidden" name="param" value="otherCostumer">
                  <button class="btn btn-excel"><i class="fa fa-file-excel-o" aria-hidden="true"></i> Export Excel</button>  
                </form>
              </div>
               <table id="otcost-tbl" class="table table-striped table-bordered" cellspacing="0" width="1800px">
                <thead>
                    <tr>
                      <th>No</th>
                      <th>Sales Name</th>
                      <th>Customer Name</th>
                      <th>Job Title</th>
                      <th>Company</th>
                      <th>Address</th>
                      <th>City</th>
                      <th>Postal Code</th>
                      <th>Phone</th>
                      <th>Fax</th>
                      <th>Email</th>
                      <th>Action</th>
                    </tr>
                  </thead>
              </table>
            </div>
            <div id="tbl-costBySales" class="table-responsive">
              <div class="head-table">
                <!-- <form action="exportExcel.php" method="POST"> -->
                  <input type="hidden" id="sess_id">
                  <input type="hidden" id="param" value="costumerBySales">
                  <input type="hidden" id="sales_name">
                  <button id="btn-costumerBySales" class="btn btn-excel"><i class="fa fa-file-excel-o" aria-hidden="true"></i> Export Excel</button>  
                <!-- </form> -->
              </div>
               <table id="costBySales-tbl" class="table table-striped table-bordered" cellspacing="0" width="1800px">
                <thead>
                    <tr>
                      <th>No</th>
                      <th>Name</th>
                      <th>Job Title</th>
                      <th>Company</th>
                      <th>Address</th>
                      <th>City</th>
                      <th>Postal Code</th>
                      <th>Phone</th>
                      <th>Fax</th>
                      <th>Email</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <!-- <tbody>
                    
                  </tbody> -->
              </table>
            </div>
          </div>
          <div id="content-home" class="head-table">
            <input type="hidden" id="session_id_global" value="<?php echo $sess_id; ?>">
            <h1>Welcome <?php echo $sess_name; ?> !</h1>
          </div>
        </main>
      </div>
    </div>

     <footer>
        <div class="container-fluid">
            <div class="footer-sign">
                <p class="text-center">© 2017. JAYA METAL TEKNIKA, PT</p>
            </div>
        </div><!-- /container-fluid -->
      </footer>

    <div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Add New Customer</h4>
            </div>
            <div class="modal-body">
            <!-- <form> -->
              <input type="hidden" id="id_sales_add" value="<?php echo $sess_id; ?>">
              <div class="col-md-12">
                <div class="form-group row">
                  <div class="col-md-12">
                    <label class="control-label">Customer Name *</label>
                    <div class="controls">
                      <input class="form-control" type="text" id="cost_name" placeholder="Input here..." >
                    </div>
                  </div>
                </div>
                <div class="form-group row">
                  <div class="col-md-12">
                    <label class="control-label">Job Title *</label>
                    <div class="controls">
                      <input class="form-control" type="text" id="job_title" placeholder="Input here..." >
                    </div>
                  </div>
                </div>
                <div class="form-group row">
                  <div class="col-md-12">
                    <label class="control-label">Company *</label>
                    <div class="controls">
                      <input class="form-control" type="text" id="company" placeholder="Input here..." >
                    </div>
                  </div>
                </div>
                <div class="form-group row">
                  <div class="col-md-12">
                    <label class="control-label">Address *</label>
                    <div class="controls">
                      <input class="form-control" type="text" id="address" placeholder="Input here..." >
                    </div>
                  </div>
                </div>
                <div class="form-group row">
                  <div class="col-md-12">
                    <label class="control-label">City *</label>
                    <div class="controls">
                      <input class="form-control" type="text" id="city" placeholder="Input here..." >
                    </div>
                  </div>
                </div>
                <div class="form-group row">
                  <div class="col-md-12">
                    <label class="control-label">Postal Code</label>
                    <div class="controls">
                      <input class="form-control" type="text" id="post_code" placeholder="Input here..." >
                    </div>
                  </div>
                </div>
                <div class="form-group row">
                  <div class="col-md-12">
                    <label class="control-label">Phone</label>
                    <div class="controls">
                      <input class="form-control" type="text" id="phone" placeholder="Input here..." >
                    </div>
                  </div>
                </div>
                <div class="form-group row">
                  <div class="col-md-12">
                    <label class="control-label">Fax</label>
                    <div class="controls">
                      <input class="form-control" type="text" id="fax" placeholder="Input here...">
                    </div>
                  </div>
                </div>
                <div class="form-group row">
                  <div class="col-md-12">
                    <label class="control-label">Email</label>
                    <div class="controls">
                      <input class="form-control" type="email" id="email" placeholder="Input here..." >
                    </div>
                  </div>
                </div>
                <div class="form-group row">
                  <div class="col-md-12">
                      <button type="button" id="addCostInput" class="btn btn-primary btn-sm">SAVE</button>
                      <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">CANCEL</button>
                  </div>
                </div>
              </div>
            <!-- </form> -->
            </div><!-- /.modal-body -->             
          </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <div class="modal fade" id="addModalExcel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Add New Customer</h4>
            </div>
            <div class="modal-body">
            <form class="well" action="importExcel.php" method="post" name="upload_excel" enctype="multipart/form-data">
              <input type="hidden" name="id_sales_excel" value="<?php echo $sess_id ?>">
              <div class="col-md-12">
                <div class="form-group row">
                  <div class="col-md-12">
                    <label class="control-label">Upload Excel (.csv) File (maximum upload 500 rows data)</label>
                    <div class="controls">
                      <input type="file" name="file" id="file" class="input-large form-control" required>
                    </div>
                  </div>
                </div>
                <div class="form-group row">
                  <div class="col-md-12">
                      <button type="submit" id="addCostExcel" name="addCostExcel" class="btn btn-primary btn-sm">UPLOAD</button>
                      <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">CANCEL</button>
                  </div>
                </div>
              </div>
            </form>
            </div><!-- /.modal-body -->             
          </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <!-- Modal edit form -->
    <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Edit My Customer</h4>
            </div>
            <div class="modal-body">
            <!-- <form> -->
              <input type="hidden" id="id_cost">
              <div class="col-md-12">
                <div class="form-group row">
                  <div class="col-md-12">
                    <label class="control-label">Customer Name</label>
                    <div class="controls">
                      <input class="form-control" type="text" id="cost_name_edit" required>
                    </div>
                  </div>
                </div>
                <div class="form-group row">
                  <div class="col-md-12">
                    <label class="control-label">Job Title</label>
                    <div class="controls">
                      <input class="form-control" type="text" id="job_title_edit" required>
                    </div>
                  </div>
                </div>
                <div class="form-group row">
                  <div class="col-md-12">
                    <label class="control-label">Company</label>
                    <div class="controls">
                      <input class="form-control" type="text" id="company_edit" required>
                    </div>
                  </div>
                </div>
                <div class="form-group row">
                  <div class="col-md-12">
                    <label class="control-label">Address</label>
                    <div class="controls">
                      <input class="form-control" type="text" id="address_edit" required>
                    </div>
                  </div>
                </div>
                <div class="form-group row">
                  <div class="col-md-12">
                    <label class="control-label">City</label>
                    <div class="controls">
                      <input class="form-control" type="text" id="city_edit" required>
                    </div>
                  </div>
                </div>
                <div class="form-group row">
                  <div class="col-md-12">
                    <label class="control-label">Postal Code</label>
                    <div class="controls">
                      <input class="form-control" type="number" id="post_code_edit">
                    </div>
                  </div>
                </div>
                <div class="form-group row">
                  <div class="col-md-12">
                    <label class="control-label">Phone</label>
                    <div class="controls">
                      <input class="form-control" type="text" id="phone_edit">
                    </div>
                  </div>
                </div>
                <div class="form-group row">
                  <div class="col-md-12">
                    <label class="control-label">Fax</label>
                    <div class="controls">
                      <input class="form-control" type="text" id="fax_edit">
                    </div>
                  </div>
                </div>
                <div class="form-group row">
                  <div class="col-md-12">
                    <label class="control-label">Email</label>
                    <div class="controls">
                      <input class="form-control" type="email" id="email_edit">
                    </div>
                  </div>
                </div>
                <div class="form-group row">
                  <div class="col-md-12">
                      <button type="submit" id="updateCost" class="btn btn-primary btn-sm">UPDATE</button>
                      <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">CANCEL</button>
                  </div>
                </div>
              </div>
            <!-- </form> -->
            </div><!-- /.modal-body -->             
          </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <div class="modal fade" id="changPwdModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Change Password</h4>
            </div>
            <div class="modal-body">
            <!-- <form> -->
              <input type="hidden" id="id_sales_pwd" value="<?php echo $sess_id ?>">
              <input type="hidden" id="pwd" value="<?php echo $sess_pwd ?>">
              <div class="col-md-12">
                <div class="form-group row">
                  <div class="col-md-12">
                    <label class="control-label">Old Password</label>
                    <div class="controls">
                      <input class="form-control" type="password" id="old_pass">
                    </div>
                  </div>
                </div>
                <div class="form-group row">
                  <div class="col-md-12">
                    <label class="control-label">New Password</label>
                    <div class="controls">
                      <input class="form-control" type="password" id="new_pass">
                    </div>
                  </div>
                </div>
                <div class="form-group row">
                  <div class="col-md-12">
                    <label class="control-label">Retype New Password</label>
                    <div class="controls">
                      <input class="form-control" type="password" id="new_pass_rt">
                    </div>
                  </div>
                </div>
                <div class="form-group row">
                  <div class="col-md-12">
                      <button type="submit" id="updatePwd" class="btn btn-primary btn-sm">CHANGE</button>
                      <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">CANCEL</button>
                  </div>
                </div>
              </div>
            <!-- </form> -->
            </div><!-- /.modal-body -->             
          </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div>

    <div class="modal fade" id="listSalesModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">List Sales</h4>
                <button type="button" class="btn btn-primary btn-sm" data-dismiss="modal"><i class="fa fa-times"></i></button>
            </div>
            <div class="modal-body">
            <!-- <form> -->
              <div class="table-responsive">
              <table id="tbl-sales-mesin" class="table table-striped table-bordered" cellspacing="0" width="500px">
                <thead>
                    <tr>
                      <th colspan="2" class="text-center">SALES MACHINE</th>
                    </tr>
                    <tr>
                      <th width="10%">No</th>
                      <th>Name</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                      require 'config.php';
                      $sql = "SELECT id_sales,name FROM sales WHERE sales_div = 'Machine' AND id_sales != $sess_id;"; 
                      $result =  mysqli_query($koneksi,$sql);
                      $no=1;
                      foreach ($result as $row){
                        echo "<tr>
                            <td>".$no."</td>
                            <td><a href='javascript:viewCostBySales(".$row['id_sales'].",\"".$row['name']."\")'>".$row['name']."</a></td>
                              </tr>";
                        $no++;
                      }
                    ?>
                  </tbody>
              </table>
            </div>
            <hr>
            <div class="table-responsive">
              <table id="tbl-sales-tool" class="table table-striped table-bordered" cellspacing="0" width="500px">
                <thead>
                    <tr>
                      <th colspan="2" class="text-center">SALES TOOL</th>
                    </tr>
                    <tr>
                      <th width="10%">No</th>
                      <th>Name</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                      require 'config.php';
                      $sql = "SELECT id_sales,name FROM sales WHERE sales_div = 'Tool' AND id_sales != $sess_id;"; 
                      $result =  mysqli_query($koneksi,$sql);
                      $no=1;
                      foreach ($result as $row){
                        echo "<tr>
                            <td>".$no."</td>
                            <td><a href='javascript:viewCostBySales(".$row['id_sales'].",\"".$row['name']."\")'>".$row['name']."</a></td>
                              </tr>";
                        $no++;
                      }
                    ?>
                  </tbody>
              </table>
            </div>
            <!-- </form> -->
            </div><!-- /.modal-body -->             
          </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div>

    <!-- Modal MyProfile form -->
    <div class="modal fade" id="myProfileModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">My Profile</h4>
            </div>
            <div class="modal-body">
            <!-- <form> -->
              <input type="hidden" id="id_cost">
              <div class="col-md-12">
                <div class="form-group row">
                  <div class="col-md-12">
                    <label class="control-label">Name :</label>
                    <div class="controls">
                      <p><?php echo $sess_name; ?></p>
                    </div>
                  </div>
                </div>
                <hr>
                <div class="form-group row">
                  <div class="col-md-12">
                    <label class="control-label">User Email :</label>
                    <div class="controls">
                      <p><?php echo $sess_email; ?></p>
                    </div>
                  </div>
                </div>
                <hr>
                <div class="form-group row">
                  <div class="col-md-12">
                    <label class="control-label">Other Email :</label>
                    <div class="controls">
                      <p><?php echo $sess_memail; ?></p>
                    </div>
                  </div>
                </div>
                <hr>
                <div class="form-group row">
                  <div class="col-md-12">
                    <label class="control-label">Sales Division :</label>
                    <div class="controls">
                      <p><?php echo $sess_div; ?></p>
                    </div>
                  </div>
                </div>
                <hr>
                <div class="form-group row">
                  <div class="col-md-12">
                    <label class="control-label">Job Title :</label>
                    <div class="controls">
                      <p><?php echo $sess_jabatan; ?></p>
                    </div>
                  </div>
                </div>
                <hr>
                <div class="form-group row">
                  <div class="col-md-12">
                    <label class="control-label">Phone Number :</label>
                    <div class="controls">
                      <p><?php echo $sess_phone; ?></p>
                    </div>
                  </div>
                </div>
                <div class="form-group row">
                  <div class="col-md-12">
                      <button type="submit" id="editProfile" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i> Edit</button>
                      <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">CANCEL</button>
                  </div>
                </div>
              </div>
            <!-- </form> -->
            </div><!-- /.modal-body -->             
          </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <!-- Modal MyProfileEdit form -->
    <div class="modal fade" id="myProfileEditModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Edit My Profile</h4>
            </div>
            <div class="modal-body">
            <!-- <form> -->
              <input type="hidden" id="id_myprof" value="<?php echo $sess_id; ?>">
              <div class="col-md-12">
                <div class="form-group row">
                  <div class="col-md-12">
                    <label class="control-label">Name</label>
                    <div class="controls">
                      <input class="form-control" type="text" id="myname_edit" value="<?php echo $sess_name; ?>" required>
                    </div>
                  </div>
                </div>
                <div class="form-group row">
                  <div class="col-md-12">
                    <label class="control-label">Other Email</label>
                    <div class="controls">
                      <input class="form-control" type="text" id="otherEmail_edit" value="<?php echo $sess_memail; ?>">
                    </div>
                  </div>
                </div>
                <div class="form-group row">
                  <div class="col-md-12">
                    <label class="control-label">Sales Division</label>
                    <div class="controls">
                      <!-- <input class="form-control" type="text" id="mydiv_edit" value="<?php echo $sess_div; ?>" required> -->
                      <select id="mydiv_edit" class="form-control">
                        <option value="Machine" <?php if ($sess_div=='Machine') {echo "selected";} ?>>Machine</option>
                        <option value="Tool" <?php if ($sess_div=='Tool') {echo "selected";} ?>>Tool</option>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="form-group row">
                  <div class="col-md-12">
                    <label class="control-label">Job Title</label>
                    <div class="controls">
                      <input class="form-control" type="text" id="myjob_edit" value="<?php echo $sess_jabatan; ?>" required>
                    </div>
                  </div>
                </div>
                <div class="form-group row">
                  <div class="col-md-12">
                    <label class="control-label">Phone Number</label>
                    <div class="controls">
                      <input class="form-control" type="number" id="myphone_edit" value="<?php echo $sess_phone; ?>" required>
                    </div>
                  </div>
                </div>
                <div class="form-group row">
                  <div class="col-md-12">
                      <button type="submit" id="updateProfile" class="btn btn-primary btn-sm">Update</button>
                      <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">CANCEL</button>
                  </div>
                </div>
              </div>
            <!-- </form> -->
            </div><!-- /.modal-body -->             
          </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <!-- Modal view detail form -->
    <div class="modal fade" id="viewModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Detail Customer</h4>
                <button type="button" class="btn btn-primary btn-sm" data-dismiss="modal"><i class="fa fa-times"></i></button>
            </div>
            <div class="modal-body">
            <!-- <form> -->
              <div class="col-md-12">
                <div class="form-group row">
                  <div class="col-md-12">
                    <label class="control-label">Customer Name</label>
                    <div class="controls">
                      <p id="cost_name_view"></p>
                    </div>
                  </div>
                </div>
                <hr>
                <div class="form-group row">
                  <div class="col-md-12">
                    <label class="control-label">Job Title</label>
                    <div class="controls">
                      <p id="job_title_view"></p>
                    </div>
                  </div>
                </div>
                <hr>
                <div class="form-group row">
                  <div class="col-md-12">
                    <label class="control-label">Company</label>
                    <div class="controls">
                      <p id="company_view"></p>
                    </div>
                  </div>
                </div>
                <hr>
                <div class="form-group row">
                  <div class="col-md-12">
                    <label class="control-label">Address</label>
                    <div class="controls">
                      <p id="address_view"></p>
                    </div>
                  </div>
                </div>
                <hr>
                <div class="form-group row">
                  <div class="col-md-12">
                    <label class="control-label">City</label>
                    <div class="controls">
                      <p id="city_view"></p>
                    </div>
                  </div>
                </div>
                <hr>
                <div class="form-group row">
                  <div class="col-md-12">
                    <label class="control-label">Postal Code</label>
                    <div class="controls">
                      <p id="post_code_view"></p>
                    </div>
                  </div>
                </div>
                <hr>
                <div class="form-group row">
                  <div class="col-md-12">
                    <label class="control-label">Phone</label>
                    <div class="controls">
                      <p id="phone_view"></p>
                    </div>
                  </div>
                </div>
                <hr>
                <div class="form-group row">
                  <div class="col-md-12">
                    <label class="control-label">Fax</label>
                    <div class="controls">
                      <p id="fax_view"></p>
                    </div>
                  </div>
                </div>
                <hr>
                <div class="form-group row">
                  <div class="col-md-12">
                    <label class="control-label">Email</label>
                    <div class="controls">
                      <p id="email_view"></p>
                    </div>
                  </div>
                </div>
              </div>
            <!-- </form> -->
            </div><!-- /.modal-body -->             
          </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    
  </body>
</html>