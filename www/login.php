<?php
   require 'config.php';
   
   session_start();
   
   if($_SERVER["REQUEST_METHOD"] == "POST") {
      // username and password sent from form 
      
      $myusername = mysqli_real_escape_string($koneksi,$_POST['email']);
      // $mypassword = mysqli_real_escape_string($koneksi,$_POST['pwd']);
      $mypassword = md5(mysqli_real_escape_string($koneksi,$_POST['pwd']));  
      
      $sql = "SELECT id_sales FROM sales WHERE email = '$myusername' and password = '$mypassword'";
      $result = mysqli_query($koneksi,$sql);
      $row = mysqli_fetch_array($result,MYSQLI_ASSOC);
      
      $count = mysqli_num_rows($result);
		
      if($count == 1) {
         $_SESSION['login_user'] = $myusername; 
         header("location: index.php");
      }else {
         $error = "Your Login Name or Password is invalid";
      }
   }
?>
<!DOCTYPE html>
<html>
<head>
	<title>Login App Customer Report</title>

	<link href="assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/css/styles.css" rel="stylesheet">
</head>
<body style="background-color: #343a40;">
	<div class="container py-5">
	    <div class="row">
	        <div class="col-md-12">
	            <h2 class="text-center text-white mb-4">Customer Report Aplication</h2>
	            <div class="row">
	                <div class="col-md-6 mx-auto">
	                    <span class="anchor" id="formLogin"></span>
	                    <!-- form card login -->
	                    <div class="card rounded-0">
	                        <div class="card-header">
	                            <h3 class="mb-0">Login</h3>
	                        </div>
	                        <div class="card-body">
	                            <form action="" method="post">
	                                <div class="form-group">
	                                    <label for="uname1">Email</label>
	                                    <input type="text" class="form-control form-control-lg rounded-0" name="email" id="email" required="" placeholder="Insert email...">
	                                </div>
	                                <div class="form-group">
	                                    <label>Password</label>
	                                    <input type="password" class="form-control form-control-lg rounded-0" name="pwd" id="pwd" required="" autocomplete="new-password" placeholder="Insert password...">
	                                </div>
	                                <button type="submit" id="login" class="btn login-btn btn-lg float-right">Login</button>
	                            </form>
	                        </div>
	                        <!--/card-block-->
	                    </div>
	                    <!-- /form card login -->
	                </div>
	            </div>
	            <!--/row-->
	        </div>
	        <!--/col-->
	    </div>
	    <!--/row-->
	</div>
<!--/container-->
    <script src="assets/js/jquery-1.12.4.js"></script>
    <script src="assets/js/jquery-3.2.1.slim.min.js"></script>
    <script src="assets/js/popper.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
</body>
</html>


